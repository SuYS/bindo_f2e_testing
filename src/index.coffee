# app static files
require './index.sass'
require './index.jade'

# config dependencies
Router.Link.defaultProps.activeClassName = 'is-active'


route = require './pages/route'
appRoot = document.getElementById 'root'
Router.run route, (Root) ->
  React.render <Root />, appRoot
