Header = require './components/header'
Menu = require './components/menu'

{RouteHandler} = Router

module.exports = React.createClass {
  render: ->
    <div className="o-page u-clearfix">
      <Header {...@props} />
      <div className="o-page__side-block">
        <Menu {...@props} />
      </div>
      <div className="o-page__main-block">
        <RouteHandler />
      </div>
    </div>
}
