UserActions = require 'actions/UserActions'
UserStore = require 'stores/UserStore'
MainLayout = require './mainLayout'

{Navigation, State, RouteHandler} = Router

module.exports = React.createClass {
  mixins: [Navigation, State]

  getInitialState: ->
    {}

  componentDidMount: ->
    UserStore.listen @onChange
    UserActions.initFromSession()

  componentWillUnmount: ->
    UserStore.unlisten @onChange

  onChange: (state) ->
    @setState state

  componentWillUpdate: (nextProps, nextState) ->
    state = (@state.isInit && @state) || (nextState.isInit && nextState)

    if state.isInit && !state.isAuth && !@isActive 'login'
      query = {next: @getPath()}
      if query.next.indexOf('/logout') == 0
        query = {}
      @transitionTo '/login', {}, query

    if state.user
      {stores} = state.user
      if @getPath() == '/' && stores.length
        @replaceWith 'inventory', {id: stores[0].id}

  render: ->
    if @state.isInit
      if @state.isAuth && @isActive 'store'
        return (
          <MainLayout user={ @state.user } >
            <RouteHandler />
          </MainLayout>
        )

      return <RouteHandler />

    <div>Loading</div>
}
