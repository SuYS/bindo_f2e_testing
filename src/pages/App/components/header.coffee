Spinner = require 'spinner'
AsyncTaskStore = require 'stores/AsyncTaskStore'

{State, Link} = Router

TasksSpinner = React.createClass {
  getInitialState: ->
    AsyncTaskStore.getState()

  componentDidMount: ->
    AsyncTaskStore.listen @onChange

  componentWillUnmount: ->
    AsyncTaskStore.unlisten @onChange

  onChange: (state) ->
    @setState state

  render: ->
    statusCls = @state.isTasksRunning && 'is-active' || ''
    <div className="o-header__spinner #{statusCls}">
      <Spinner />
    </div>
}

StoreList = React.createClass {
  mixins: [State]

  render: ->
    <ol className="o-store">
      {
        routers = @getRoutes()
        if routers.length >= 3
          routerName = routers[2].name
        routerName = routerName || 'inventory'
        @props.user.stores.map (store) ->
          params = {id: store.id}
          <li className="o-store__item" key={ store.id }>
            <Link className="o-store__link" to={ routerName } params={ params }>{ store.name }</Link>
          </li>
      }
    </ol>

}

User = React.createClass {
  render: ->
    avatarStyle = {
      backgroundImage: "url(#{@props.user.avatar})"
    }

    <div className="o-user">
      <span className="o-user__avatar u-align-middle" style={ avatarStyle } />
      <span className="o-user__actions u-align-middle">
        <span className="o-user__name">Hello { @props.user.nickname }</span>
        <Link className="o-user__logout" to="logout">Log out</Link>
      </span>
    </div>
}


Header = React.createClass {
  render: ->
    <header className="o-header">
      <div className="o-header__title">Bindo</div>
      <StoreList {...@props} />
      <TasksSpinner />
      <User {...@props} />
    </header>
}


module.exports = Header
