{Link, State} = Router

Item = React.createClass {
  mixins: [State]
  render: ->
    params = @getParams()
    <li className="o-menu__item">
      <Link className="o-menu__link" to={ @props.to } params={ params }>{ @props.text }</Link>
    </li>
}

Menu = React.createClass {
  render: ->
    <ul className="o-menu">
      <Item to="sales" text="Sales" />
      <Item to="inventory" text="Inventory" />
      <Item to="suppliers" text="Suppliers" />
      <Item to="customers" text="Customers" />
    </ul>
}

module.exports = Menu
