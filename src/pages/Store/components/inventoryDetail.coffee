Item = React.createClass {
  render: ->
    {label, formatter} = @props
    value = @props.item[label.toLowerCase()]

    if typeof formatter == 'function'
      value = formatter value

    <li className="o-inventory-detail__item">
      <h5 className="o-inventory-detail__label">{ @props.label }</h5>
      <div className="o-inventory-detail__value">{ value }</div>
    </li>
}

toDollor = (value) -> "$#{value}"

labelList = [
  'Name'
  'Barcode'
  'Quantity'
  {
    label: 'Price'
    formatter: toDollor
  }
  {
    label: 'Cost'
    formatter: toDollor
  }
  {
    label: 'Tax'
    formatter: toDollor
  }
  'Brand'
  {
    label: 'Discount'
    formatter: (value) -> "#{value * 100}% off"
  }
  'Note'
]

module.exports = React.createClass {
  render: ->
    {item} = @props
    <ul className="o-inventory-detail">
      {
        labelList.map (label) ->
          if typeof label != 'string'
            formatter = label.formatter
            label = label.label
          <Item key={ label } label={ label } item={ item } formatter={ formatter } />
      }
    </ul>
}
