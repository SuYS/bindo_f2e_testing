tableHeaderLabel = {
  barcode: 'Barcode'
  name: 'Name'
  quantity: 'Quantity'
  cost: 'Cost'
  category: 'Category'
}

Row = React.createClass {
  getDefaultProps: ->
    {
      className: ''
      onRowSelected: ->
    }

  handleRowClick: (e) ->
    index = Number e.currentTarget.getAttribute('data-index')
    @props.onRowSelected index

  render: ->
    {item} = @props
    <li className="o-inventory-table__row #{@props.className}" data-index={ @props.index } onClick={ @handleRowClick }>
      <ol className="o-goods-list">
        <li className="o-goods-list__item">{ item.barcode }</li>
        <li className="o-goods-list__item">{ item.name }</li>
        <li className="o-goods-list__item u-mode-locator">{ item.quantity }</li>
        <li className="o-goods-list__item">{ item.cost }</li>
        <li className="o-goods-list__item">{ item.category }</li>
      </ol>
    </li>
}

module.exports = React.createClass {
  render: ->
    <ol className="o-inventory-table">
      <Row className="o-inventory-table__row--header" item={ tableHeaderLabel } />
      {
        @props.goods.map (item, i) =>
          rowCls = i == @props.selectedIndex && 'is-selected' || ''
          <Row className={ rowCls } key={ item.sn } item={ item } index={ i } onRowSelected={ @props.onRowSelected } />
      }
    </ol>
}
