{State, Link} = Router

getPageRange = (page, size) ->
  if size == 0
    return [0, 0]

  max = size - 1
  start = Math.max page - 2, 0
  end = Math.min page + 2, max
  length = end - start

  if length < 4
    diff = 4 - length
    if start == 0
      end = Math.min end + diff, max
    else
      start = Math.max start - diff , 0

  [start, end]


PageItem = React.createClass {
  getDefaultProps: ->
    {
      className: ''
    }

  mixins: [State]

  render: ->
    {page} = @props
    params = @getParams()
    query = {page}
    activeCls = page == @props.currentPage && 'is-active' || ''

    <li className="o-pagination__item #{activeCls} #{@props.className}">
      <Link className="o-pagination__link" to={ @props.to } params={ params } query={ query }>{ (page + 1) }</Link>
    </li>
}


Pagination = React.createClass {
  mixins: [State]

  getDefaultProps: ->
    {
      total: 10
    }

  render: ->
    currentPage = Number(@getQuery().page) || 0
    size = Math.ceil @props.total / 3
    [start, end] = getPageRange currentPage, size

    itemProps = {
      to: @props.to
      currentPage: currentPage
    }

    <ol className="o-pagination">
      {
        if start != 0
          if start - 0 > 1
            startCls = 'o-pagination__item--more'
          <PageItem {...itemProps} page={ 0 } className={ startCls } />
      }
      {
        for i in [start..end]
          <PageItem {...itemProps} page={ i } key={ i } />
      }
      {
        endDiff = size - end
        if endDiff > 1
          if endDiff > 2
            endCls = 'o-pagination__item--more'
          <PageItem {...itemProps} page={ size - 1 } className={ endCls } />
      }
    </ol>
}


module.exports = Pagination
