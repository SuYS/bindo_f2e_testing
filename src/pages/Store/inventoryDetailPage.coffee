InventoryDetail = require './components/inventoryDetail'
InventoryStore = require 'stores/InventoryStore'

module.exports = React.createClass {
  mixins: [Router.Navigation]

  getInitialState: ->
    {
      item: {}
    }

  componentDidMount: ->
    InventoryStore.listen @retrieveItem
    @retrieveItem()

  componentWillUnmount: ->
    InventoryStore.unlisten @retrieveItem

  retrieveItem: ->
    item = InventoryStore.getItemBySn @props.params.sn
    if item
      @setState {item}

  handleGoBack: (e) ->
    e.preventDefault()
    @goBack() || @transitionTo 'inventory', @props.params
    return

  render: ->
    {item} = @state

    <div className="s-view-mode--detail">
      <div className="o-panel">
        <h3 className="o-panel__title">Inventory #{ @props.params.sn }</h3>
        <a className="o-panel__back-link" href onClick={ @handleGoBack }>Back to list</a>
      </div>
      <div className="o-main-content">
        <InventoryDetail item={ item } />
      </div>
    </div>
}
