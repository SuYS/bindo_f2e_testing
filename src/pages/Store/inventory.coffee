Pagination = require './components/pagination'
InventoryTable = require './components/inventoryTable'
InventoryDetail = require './components/inventoryDetail'

InventoryActions = require 'actions/InventoryActions'
InventoryStore = require 'stores/InventoryStore'

{Navigation, State, RouteHandler} = Router

module.exports = React.createClass {
  mixins: [Navigation, State]

  statics: {
    willTransitionTo: (transition, params, query) ->
      InventoryActions.fetch {
        id: Number params.id
        page: Number(query.page) || 0
      }
  }

  getInitialState: ->
    state = InventoryStore.getState()
    state.mode = 'list'
    state.selectedIndex = 0
    state

  componentDidMount: ->
    InventoryStore.listen @onChange

  componentWillUnmount: ->
    InventoryStore.unlisten @onChange

  onChange: (state) ->
    @setState state

  isColumnMode: ->
    @state.mode == 'column'

  handleModeChange: (e) ->
    mode = e.target.value
    if e.target.value != @state.mode
      @setState {mode}

  onRowSelected: (selectedIndex) ->
    if @isColumnMode()
      if selectedIndex != @state.selectedIndex
        @setState {selectedIndex}
    else
      item = @state.goods[selectedIndex]
      @transitionTo 'inventoryDetail', {
        id: @props.params.id
        sn: item.sn
      }

  render: ->
    if @isActive 'inventoryDetail'
      return <RouteHandler />

    modeCls = "s-view-mode--#{@state.mode}"
    <div className={ modeCls }>
      <div className="o-panel">
        <h3 className="o-panel__title">Inventory</h3>
        <div onChange={ @handleModeChange }>
          <label>
            <input className="o-panel__mode-option" type="radio" name="viewMode" value="list" defaultChecked={ true } />
            <span className="o-panel__mode-label">List</span>
          </label>
          <label>
            <input className="o-panel__mode-option" type="radio" name="viewMode" value="column" />
            <span className="o-panel__mode-label">Column</span>
          </label>
        </div>
        <Pagination to="inventory" total={ @state.total } />
      </div>
      <div className="o-main-content">
        <InventoryTable {...@state} onRowSelected={ @onRowSelected } />
        {
          if @state.total && @isColumnMode()
            item = @state.goods[@state.selectedIndex]
            <InventoryDetail item={ item } />
        }
      </div>
    </div>
}
