Unit = React.createClass {
  render: ->
    <li className="o-spinner__unit o-spinner__unit-#{@props.no}" />
}

module.exports = React.createClass {
  render: ->
    <ol className="o-spinner">
      {
        for i in [1..5]
          <Unit key={ i } no={ i } />
      }
    </ol>
}
