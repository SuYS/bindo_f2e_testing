Spinner = require 'spinner'
UserActions = require 'actions/UserActions'
UserStore = require 'stores/UserStore'

{Navigation, State} = Router

FieldRow = React.createClass {
  render: ->
    <div className="o-form__field">{ @props.children }</div>
}

Field = React.createClass {
  getInputDOMNode: ->
    @getDOMNode().querySelector '.o-form__input-text'

  render: ->
    <FieldRow>
      <input className="o-form__input-text" type="text" {...@props} />
    </FieldRow>
}

module.exports = React.createClass {
  mixins: [Navigation, State]

  getInitialState: ->
    {
      username: null
      password: null
      isWaiting: false
    }

  componentDidMount: ->
    @onChange UserStore.getState()
    UserStore.listen @onChange

  componentWillUnmount: ->
    UserStore.unlisten @onChange

  onChange: (state) ->
    if state.isInit && state.isAuth
      @replaceWith @getQuery().next || '/'
    else if state.isError
      passInput = @refs.password.getInputDOMNode()
      passInput.value = ''
      passInput.focus()
      state.password = ''

    state.isWaiting = false
    @setState state


  handleFileChange: (e) ->
    {target} = e
    fieldState = {}
    fieldState[target.name] = target.value
    @setState fieldState

  handleLogin: (e) ->
    e.preventDefault()
    UserActions.login @state
    @setState {
      isWaiting: true
    }

  render: ->
    isDisable = @state.isWaiting || !(@state.username?.length && @state.password?.length)
    submitText = @state.isWaiting && ''

    <div className="o-login">
      <h1 className="o-login__title">Bindo</h1>
      <form className="o-form" onSubmit={ @handleLogin }>
        <Field name="username" ref="username" autoFocus placeholder="Username" onChange={ @handleFileChange }>
          <span className="o-form__tip">Tip: pota</span>
        </Field>
        <Field name="password" ref="password" type="password" placeholder="Password" onChange={ @handleFileChange }>
          <span className="o-form__tip">Tip: cute</span>
        </Field>
        <FieldRow>
          <button className="o-form__button" type="submit" disabled={ isDisable }>Login</button>
        </FieldRow>
      </form>
      <div className="o-login__status">
        {
          if @state.isWaiting
            <Spinner />
        }
        {
          if !@state.isWaiting && @state.isError
            <div className="o-login__error-msg">{ @state.errorMessage }</div>
        }
      </div>
    </div>
}
