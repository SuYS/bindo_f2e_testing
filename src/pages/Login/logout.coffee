UserActions = require 'actions/UserActions'
UserStore = require 'stores/UserStore'

{Navigation} = Router

module.exports = React.createClass {
  mixins: [Navigation]

  componentDidMount: ->
    UserStore.listen @onChange
    UserActions.logout()

  componentWillUnmount: ->
    UserStore.unlisten @onChange

  onChange: (state) ->
    @setState state
    if !state.isAuth
      @replaceWith 'login'

  render: ->
    <div className="u-center-message">Logout processing</div>
}
