{Route, DefaultRoute} = Router

App = require './App'
Store = require './Store'
Sales = require './Store/sales'
Inventory = require './Store/inventory'
InventoryDetail = require './Store/inventoryDetailPage'
Suppliers = require './Store/suppliers'
Customers = require './Store/customers'
Login = require './Login'
Logout = require './Login/logout'

module.exports = (
  <Route path="/" handler={ App }>
    <Route name="store" path="store" handler={ Store }>
      <Route name="sales" path=":id/sales" handler={ Sales } />
      <Route name="inventory" path=":id/inventory" handler={ Inventory }>
        <Route name="inventoryDetail" path=":sn" handler={ InventoryDetail } />
      </Route>
      <Route name="suppliers" path=":id/suppliers" handler={ Suppliers } />
      <Route name="customers" path=":id/customers" handler={ Customers } />
    </Route>
    <Route name="login" handler={ Login } />
    <Route name="logout" handler={ Logout } />
  </Route>
)
