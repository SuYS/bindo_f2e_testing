alt = require 'alt'
InventoryActions = require 'actions/InventoryActions'

module.exports = alt.createStore {
  displayName: 'InventoryStore'

  bindListeners: {
    handleFetch: InventoryActions.FETCH
  }

  state: {
    goods: []
    total: 0
  }

  handleFetch: (inventory) ->
    @setState inventory

  publicMethods: {
    getItemBySn: (sn) ->
      for item in @getState().goods
        if item.sn == sn
          return item
      null
  }
}
