alt = require 'alt'
AsyncTaskActions = require 'actions/AsyncTaskActions'

module.exports = alt.createStore {
  displayName: 'AsyncTaskActions'

  bindListeners: {
    handleTaskStart: AsyncTaskActions.PUT_TASK
    handleTaskDone: AsyncTaskActions.SIGNAL_TASKS_DONE
  }

  state: {
    isTasksRunning: false
  }

  handleTaskStart: ->
    @setState {
      isTasksRunning: true
    }

  handleTaskDone: ->
    @setState {
      isTasksRunning: false
    }
}
