alt = require 'alt'
UserActions = require 'actions/UserActions'

KEY_USER = '_user_profile_'

module.exports = alt.createStore {
  displayName: 'UserStore'

  bindListeners: {
    handleInit: UserActions.INIT_FROM_SESSION
    handleLogin: UserActions.LOGIN
    handleLogout: UserActions.LOGOUT
    handleError: UserActions.THROW_ERROR
  }

  state: {
    isInit: false
    user: null
    isAuth: false
    isError: false
    errorMessage: ''
  }

  setSigninState: (user) ->
    @setState {
      user: user
      isAuth: true
      isError: false
      errorMessage: ''
    }

  handleInit: ->
    user = sessionStorage.getItem KEY_USER
    if user
      @setSigninState JSON.parse(user), true

    @setState {
      isInit: true
    }

  handleLogin: (user) ->
    sessionStorage.setItem KEY_USER, JSON.stringify(user)
    @setSigninState user

  handleLogout: ->
    sessionStorage.removeItem KEY_USER
    @setState {
      user: null
      isAuth: false
      isError: false
      errorMessage: ''
    }

  handleError: (errorMessage) ->
    @setState {
      isError: true
      errorMessage: errorMessage
    }
}
