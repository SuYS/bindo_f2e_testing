alt = require 'alt'
Api = require '../apiUtils'

userActions = alt.createActions {
  displayName: 'UserActions'

  initFromSession: ->
    @dispatch()

  login: (data) ->
    Api.post '/user', data
      .then (user) =>
        @dispatch user
      , (errorMessage) =>
        @actions.throwError errorMessage

  logout: ->
    Api.del '/user'
      .then =>
        @dispatch()

  throwError: (errorMessage) ->
    @dispatch errorMessage
}

module.exports = userActions
