alt = require 'alt'
Api = require '../apiUtils'

userActions = alt.createActions {
  displayName: 'InventoryActions'

  fetch: (query) ->
    Api.get '/inventory', query
      .then (inventory) =>
        @dispatch inventory
}

module.exports = userActions
