Promise = require 'bluebird'
alt = require 'alt'

class TasksMonitor
  constructor: (tasks, callback) ->
    @isDropped = false
    @promise = Promise.all(tasks)
      .catch ->
        null
      .finally =>
        if !@isDropped
          callback()

  drop: ->
    @isDropped = true


tasks = []
monitor = null

asyncTaskActions = alt.createActions {
  displayName: 'AsyncTaskActions'

  putTask: (task) ->
    @dispatch()

    tasks.push task
    monitor?.drop()
    monitor = new TasksMonitor tasks, =>
      monitor = null
      tasks.length = 0
      @actions.signalTasksDone()
    task

  signalTasksDone: ->
    @dispatch()
}

module.exports = asyncTaskActions
