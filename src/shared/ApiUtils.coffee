Promise = require 'bluebird'
Request = require './MockRequest'
AsyncTaskActions = require 'actions/AsyncTaskActions'

decoratePromise = (request) ->
  task = new Promise (resolve, reject) ->
    request
      .set 'Accept', 'application/json'
      .withCredentials()
      .end (error, response) ->
        if error
          return reject error

        if response.body.errorMessage
          return reject response.body.errorMessage

        resolve response.body

  AsyncTaskActions.putTask task


get = (path, query) ->
  request = Request
    .get path
    .query query
  decoratePromise request

post = (path, data) ->
  request = Request
    .post path
    .send data
  decoratePromise request


del = (path) ->
  request = Request.del path
  decoratePromise request


module.exports = {get, post, del}
