# Dummy RESTful data
handlerMap = {
  get: {
    '/inventory': (query) ->
      if query.id == 1
        data = {
          total: 74
          goods: [
            {
              sn: '0001'
              category: 'woman'
              barcode: '1231923'
              brand: 'KFB'
              name: 'Nice Dress'
              quantity: 98
              price: 100
              cost: 45
              tax: 0
              discount: .1
              note: 'note aaa'
            }
            {
              sn: '0002'
              category: 'girl'
              barcode: '1912323'
              brand: 'KFB'
              name: 'Normal Dress'
              quantity: 67
              price: 70
              cost: 40
              tax: 0
              discount: .05
              note: 'note bbb'
            }
            {
              sn: '0003'
              category: 'ladies'
              barcode: '2912313'
              brand: 'MAR'
              name: 'Rare Dress'
              quantity: 7
              price: 799
              cost: 749
              tax: 10
              discount: .15
              note: 'note ccc'
            }
          ]
        }
      else if query.id == 9
        data = {
          total: 54
          goods: [
            {
              sn: '1001'
              category: 'toy'
              barcode: '3542482'
              brand: 'Disney'
              name: 'Toy Brick'
              quantity: 89
              price: 20
              cost: 15
              tax: 0
              discount: .05
              note: 'note ddd'
            }
            {
              sn: '1002'
              category: 'toy'
              barcode: '2435842'
              brand: 'Disney'
              name: 'Stitch Lego'
              quantity: 76
              price: 80
              cost: 70
              tax: 0
              discount: .1
              note: 'note eee'
            }
            {
              sn: '1003'
              category: 'weapon'
              barcode: '4248253'
              brand: 'Blizzard'
              name: 'Epic Buriza-Do Kyanon'
              quantity: 3
              price: 899
              cost: 799
              tax: 13
              discount: .01
              note: 'note fff'
            }
          ]
        }

      reorderTargetIdx = query.page % data.goods.length
      picked = data.goods.splice(reorderTargetIdx, 1)[0]
      data.goods.unshift picked
      data
  }

  post: {
    '/user': (query, data) ->
      if data.username == 'pota' && data.password == 'cute'
        return {
          username: 'pota'
          nickname: 'Pota'
          avatar: '//i.imgur.com/liTHFYp.jpg'
          stores: [
            {
              id: 1
              name: 'Store A'
            }
            {
              id: 9
              name: 'Store B'
            }
          ]
        }
      {errorMessage: 'Incorrect Username or Password'}
  }

  del: {
    '/user': {}
  }
}


# Mock SuperAgent's request APIs
# (https://github.com/visionmedia/superagent)
class Reuest
  constructor: (@url, @handlerType) ->
  query: (@query) -> @
  send: (@data) -> @
  set: -> @
  withCredentials: -> @
  end: (callback) ->
    setTimeout =>
      handler = handlerMap[@handlerType][@url]

      if typeof handler == 'function'
        responseData = handler @query, @data
      else
        responseData = handler

      callback null, {body: responseData}
    , 1000

    @


module.exports = {
  get: (url) -> new Reuest url, 'get'
  post: (url) -> new Reuest url, 'post'
  del: (url) -> new Reuest url, 'del'
}
