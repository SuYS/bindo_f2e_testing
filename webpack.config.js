var webpack = require('webpack'),
    path = require('path'),
    ExtractTextPlugin = require('extract-text-webpack-plugin'),
    autoprefixer = require('autoprefixer-core');

var defaultConfig = {
  devServer: {
    contentBase: './public'
  },
  entry: {
    app: './src/index',
    libs: [
      'react',
      'react-router',
      'alt',
      'bluebird',
      'superagent'
    ]
  },
  output: {
    path: path.join(__dirname, 'public'),
    filename: 'bundle.js'
  },
  resolveLoader: {
    modulesDirectories: ['node_modules']
  },
  resolve: {
    modulesDirectories: ['shared', 'node_modules'],
    extensions: ['', '.js', '.jsx', '.coffee', '.cjsx']
  },
  module: {
    loaders: [
      { test: /\.js$/, exclude:[/node_modules/], loader: 'babel'},
      { test: /\.jsx$/, exclude:[/node_modules/], loader: 'jsx-loader'},
      { test: /\.coffee$/, loader: 'coffee!cjsx' },
      {
        test: /\.sass$/,
        loader: ExtractTextPlugin.extract(
          'style',
          'css?sourceMap!postcss-loader!sass?sourceMap&indentedSyntax&outputStyle=expanded'
        )
      },
      { test: /.*\.html$/, loader: 'file?name=[name].[ext]' },
      {
        test: /.*\.jade$/,
        loader: 'file?name=[name].html!jade-html'
      }
    ]
  },
  postcss: [
    autoprefixer({ browsers: ['last 2 versions', 'ie >= 10'] })
  ],
  plugins: [
    new webpack.optimize.CommonsChunkPlugin({
      name: 'libs',
      filename: 'libs.js',
      minChunks: Infinity
    }),
    new ExtractTextPlugin('style.css', {
      allChunks: true
    }),
    new webpack.ProvidePlugin({
      React: 'react',
      Router: 'react-router'
    })
  ]
};

module.exports = defaultConfig;
