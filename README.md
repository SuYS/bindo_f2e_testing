# Bindo F2E Testing

- [Online demo site](https://dl.dropboxusercontent.com/u/11244197/bindo/index.html)
- [Download source code and compiled result](https://dl.dropboxusercontent.com/u/11244197/bindo/demo.zip)

-----

## About testing

- Time spent: about 2 working days
- I'm not good at visual design, and I usually build all UI style by hand.
- I'm not setup a Node.js server, but simulate 1s delay by mocking [SuperAgent](https://github.com/visionmedia/superagent)'s request APIs
- The Pagination component is functional, but due to the dummy data, you will see many duplicated data item
- I built an async tasks monitor to spy all parallel request, to ensure that the global loading state is correct
- Single Page Application
- Use SessionStorage to simulate sign-in and sign-out
- Finally, I'm not good at UX design, so I just implemented the testing by following the wireframe

-----

## Project structure

```
.
├── package.json
|     └─  define node package dependencies and npm scripts
├── src/
|     └─  App source code
└── public/
```

-----

## Technology Stack

Source

- HTML: Jade
- CSS: SASS
- JavaScript: CoffeeScript
- [React.js](https://facebook.github.io/react/docs/getting-started.html) - a view library
- [React-router v0.13.3](http://rackt.github.io/react-router/) - handle client-side routing
- [Alt](alt.js.org) - one of a [FLUX](https://facebook.github.io/flux/docs/overview.html) implementation
- [bluebird](https://github.com/petkaantonov/bluebird) - Promise library

Bundle source code

- [Webpack](webpack.github.io) - for commonJS style module bundling

## Source Directory Structure

```
.
├── pages/
|    ├─ [App Name]/
|    |    └─ components/
|    └─ shared/
├── shared/
|    ├─ actions/
|    └─ stores/
├── style/
└── index.coffee

```

- pages - all application partial pages
- components - *React* components
- shared - common *React* components
- actions - *FLUX* actions
- stores - *FLUX* stores
- style - sass files
- index.coffee - webapp entry point